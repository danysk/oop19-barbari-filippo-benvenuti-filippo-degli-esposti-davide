package controller.files;

/**
 * Enum of all the file types
 * 
 * @author Emanuele Lamagna
 *
 */
public enum FileTypes {
	
	STATS,
	BOOSTS
	
}
