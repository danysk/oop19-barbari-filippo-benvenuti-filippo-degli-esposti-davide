package controller.files;

/**
 * Enum of all the boost types
 * 
 * @author Emanuele Lamagna
 *
 */
public enum BoostsTypes {

	FRECKLES,
	STRIPED_VERTICAL,
	STRIPED_HORIZONTAL,
	WRAPPED
	
}
