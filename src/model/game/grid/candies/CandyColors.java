package model.game.grid.candies;

/**
 * The list of all possible {@link Candy} colors.
 * @author Filippo Benvenuti
 *
 */
public enum CandyColors {
	RED,
	ORANGE,
	YELLOW,
	GREEN,
	BLUE,
	PURPLE,
	FRECKLES,
	CHOCOLATE
}
